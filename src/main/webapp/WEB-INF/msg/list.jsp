<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<jsp:include page="../commun/header.jsp">
	<jsp:param name="navActive" value="msgList" />
	<jsp:param name="titre" value="messages liste" />
</jsp:include>
<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
	<div class="row">
		<div class="col-md-5">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-comment"></span> Chat
				</div>
				<div class="panel-body">
					<ul class="chat" id="listaff" style="list-style: none;padding-left: 0px;">

					</ul>
				</div>
				<div class="panel-footer">
					<div class="input-group">
						<textarea id="msg" name="msg" class="form-control input-sm"></textarea>
						<span class="input-group-btn">
							<button class="btn btn-warning btn-sm" id="btn-chat"
								onclick="ajoutMess(event)">Envoyer</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function refresh() {
		$
				.ajax({
					url : "/msg/",
					success : function(retour) {
						var rl = retour.length;
						$('#listaff').empty();
						for (var i = 0; i < rl; i++) {
							var val = retour[i].value;
							var name = retour[i].user.name;
							if (i % 2 == 0) {
								$('#listaff')
										.append(
												"<li class=\"right clearfix\"><span class=\"chat-img pull-right\"><img src=\"http://placehold.it/50/FA6F57/fff&text=ME\" alt=\"User Avatar\" class=\"img-circle\" /></span><div class=\"chat-body clearfix\"><div class=\"header\"><small class=\" text-muted\"><span class=\"glyphicon glyphicon-time\"></span>13 mins ago</small><strong class=\"pull-right primary-font\">"
														+ name
														+ "</strong></div><p>"
														+ val
														+ "</p></div></li>");
							} else {
								$('#listaff')
										.append(
												"<li class=\"left clearfix\"><span class=\"chat-img pull-left\"><img src=\"http://placehold.it/50/55C1E7/fff&text=U\" alt=\"User Avatar\" class=\"img-circle\" /></span><div class=\"chat-body clearfix\"><div class=\"header\"><strong class=\"primary-font\">"
														+ name
														+ "</strong> <small class=\"pull-right text-muted\"><span class=\"glyphicon glyphicon-time\"></span>12 mins ago</small></div><p>"
														+ val
														+ "</p></div></li>");
							}
						}
					}
				});
	}

	setInterval(refresh, 2000);

	function ajoutMess(e) {
		console.log("Hello !")
		var msg = document.getElementById('msg').value;
		console.log(msg);
		if (msg != null) {
			reponse = $.ajax({
				url : "/msg/",
				type : "POST",
				dataType : "json",
				data : "msg=" + msg,
				success : function(code_html, status) {
					refresh();
				}
			});
			console.log(reponse);

		}
		e.preventDefault();
	}
</script>
<%@include file="../commun/footer.jsp"%>
