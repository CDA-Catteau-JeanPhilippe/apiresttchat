<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@include file="taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>${param.titre}</title>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Cda-Tchat</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item active"><a class="nav-link"
					href="index.html">Tchat <span class="sr-only">(current)</span>
				</a></li>

				<sec:authorize access="hasRole('ADMIN')">
					<li class="nav-item active"><a class="nav-link"
					href="user.html">Users <span class="sr-only">(current)</span>
				</a></li>
				</sec:authorize>
				<sec:authorize access="!isAuthenticated()">
					<li class="nav-item"><a
						class="nav-link ${param.navActive eq 'login'?'active':'' }"
						href="/login.html">login</a></li>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					<li class="nav-item"><a class="nav-link " href="/logout">logout</a>
					</li>
				</sec:authorize>
				<sec:authorize access="hasAnyRole('USER','ADMIN')">
					<li>
						<p style="color: #FFFFFF;">
							<sec:authentication property="principal.username" />
						</p>
					</li>
				</sec:authorize>
			</ul>
		</div>
	</nav>