package com.afpa.cda.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {
	public List<Message> findAll();
}