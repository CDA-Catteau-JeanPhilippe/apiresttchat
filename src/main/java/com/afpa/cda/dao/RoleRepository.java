package com.afpa.cda.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
	public List<Role> findAll();

	@Query("SELECT r FROM Role r WHERE r.name=:roleName")
	public Optional<Role> findByName(String roleName);
}