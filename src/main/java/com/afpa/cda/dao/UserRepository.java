package com.afpa.cda.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	
	@Query("select u from User u where u.name != 'admin'")
	public List<User> findAll();
	
	@Query("SELECT u FROM User u WHERE u.name=:username")
	public Optional<User> findByName(String username);
}
