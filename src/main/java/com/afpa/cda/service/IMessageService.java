package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.MessageDto;

public interface IMessageService {
	public List<MessageDto> trouverTousLesMessages();

	public MessageDto ajouter(MessageDto message);
}
