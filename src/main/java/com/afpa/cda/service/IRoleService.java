package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.RoleDto;

public interface IRoleService {
	public List<RoleDto> trouverTousLesRoles();

	public Optional<RoleDto> trouverParLeNom(String nomRole);
	
	public Boolean ajouter(RoleDto role);
}
