package com.afpa.cda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.RoleRepository;
import com.afpa.cda.dto.RoleDto;
import com.afpa.cda.entity.Role;

@Service
public class RoleServiceImpl implements IRoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public List<RoleDto> trouverTousLesRoles() {
		List<Role> listRole = roleRepository.findAll();
		List<RoleDto> listRoleDto = new ArrayList<RoleDto>();
		for (Role role : listRole) {
			listRoleDto.add(RoleDto.builder().id(role.getId()).name(role.getName()).build());
		}
		return listRoleDto;
	}

	@Override
	public Optional<RoleDto> trouverParLeNom(String nomRole) {
		Optional<Role> roleOp = roleRepository.findByName(nomRole);
		Optional<RoleDto> roleDtoOp = null;
		if (roleOp.isPresent()) {
			roleDtoOp = Optional.of(RoleDto.builder().id(roleOp.get().getId()).name(roleOp.get().getName()).build());
		}
		return roleDtoOp;
	}

	@Override
	public Boolean ajouter(RoleDto roledto) {
		this.roleRepository.save(Role.builder().name(roledto.getName()).build());
		return true;
	}
	
}
