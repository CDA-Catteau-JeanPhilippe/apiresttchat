package com.afpa.cda.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.UserRepository;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.User;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<UserDto> trouverTousLesUtilisateurs() {
		List<User> listUser = userRepository.findAll();
		List<UserDto> listUserDto = new ArrayList<UserDto>();
		for (User user : listUser) {
			listUserDto.add(modelMapper.map(user, UserDto.class));
		}
		return listUserDto;
	}

	@Override
	public Optional<UserDto> trouverParLeNomDUtilisateur(String nomUtilisateur) {
		Optional<User> userOp = userRepository.findByName(nomUtilisateur);
		Optional<UserDto> userDtoOp;
		if (userOp.isPresent()) {
			userDtoOp = Optional.of(modelMapper.map(userOp.get(), UserDto.class));
		} else {
			userDtoOp = null;
		}
		return userDtoOp;
	}

	@Override
	public Optional<UserDto> trouverParLID(Integer idUtilisateur) {
		Optional<User> userOp = userRepository.findById(idUtilisateur);
		Optional<UserDto> userDtoOp;
		if (userOp.isPresent()) {
			userDtoOp = Optional.of(modelMapper.map(userOp.get(), UserDto.class));
		} else {
			userDtoOp = null;
		}
		return userDtoOp;
	}

	@Override
	public UserDto ajouter(UserDto userDto) {
		return modelMapper.map(this.userRepository.save(modelMapper.map(userDto, User.class)), UserDto.class);
	}

	@Override
	public void supprimerParLID(Integer idUtilisateur) {
		if(this.userRepository.findById(idUtilisateur).isPresent()) {
			this.userRepository.deleteById(idUtilisateur);
		}
	}

}
