package com.afpa.cda.service;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class Hashage {
	public static String hashSalt(String password) {
		return BCrypt.hashpw(password, BCrypt.gensalt());
	}
}
