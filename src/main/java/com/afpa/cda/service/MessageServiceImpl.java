package com.afpa.cda.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.MessageRepository;
import com.afpa.cda.dto.MessageDto;
import com.afpa.cda.entity.Message;

@Service
public class MessageServiceImpl implements IMessageService {

	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public List<MessageDto> trouverTousLesMessages() {
		List<Message> listMessage = messageRepository.findAll();
		List<MessageDto> listMessageDto = new ArrayList<MessageDto>();
		for (Message message : listMessage) {
			listMessageDto.add(this.modelMapper.map(message, MessageDto.class));
		}
		return listMessageDto;
	}

	@Override
	public MessageDto ajouter(MessageDto messageDto) {
		return modelMapper.map(this.messageRepository.save(this.modelMapper.map(messageDto, Message.class)), MessageDto.class);
	}

}
