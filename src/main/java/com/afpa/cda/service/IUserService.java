package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.UserDto;

public interface IUserService {
	public List<UserDto> trouverTousLesUtilisateurs();
	
	public Optional<UserDto> trouverParLeNomDUtilisateur(String nomUtilisateur);
	
	public Optional<UserDto> trouverParLID(Integer idUtilisateur);

	public UserDto ajouter(UserDto user);

	public void supprimerParLID(Integer idUtilisateur);
}
