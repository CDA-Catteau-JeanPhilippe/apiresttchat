package com.afpa.cda;

import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.afpa.cda.dao.MessageRepository;
import com.afpa.cda.dao.RoleRepository;
import com.afpa.cda.dao.UserRepository;
import com.afpa.cda.entity.Message;
import com.afpa.cda.entity.Role;
import com.afpa.cda.entity.User;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class RestWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestWebApplication.class, args);
	}
	
	@Bean
	public ModelMapper createModelMapper() {
		return new ModelMapper();
	}

	@Bean
    public CommandLineRunner init (RoleRepository roleDao,UserRepository userDao, MessageRepository messageDao, BCryptPasswordEncoder bcryp){
        return args -> {
        	
        	if(! roleDao.findByName("USER").isPresent()) {
        		roleDao.save(Role.builder().name("USER").build());
        	}
        	
        	Role adminRole = null;
        	if((adminRole = roleDao.findByName("ADMIN").orElse(null)) == null) {
        		adminRole = roleDao.save(Role.builder().name("ADMIN").build());
        	}
        	
        	User userAdmin = null;
            if((userAdmin = userDao.findByName("admin").orElse(null)) == null) {
            	String adminPassword = "cdatest";
            	StringBuilder sb = new StringBuilder();
            	sb.append("\n\n*****\n\n")
            		.append("insertion compte admin ...")
            		.append("\n")
            		.append("mot de passe : "+adminPassword)
            		.append("\n")
            		.append("\n\n*****\n");
            	log.error(sb.toString());
            	
            	userAdmin = userDao.save(User.builder()
            			.name("admin")
            			.password(bcryp.encode(adminPassword))
            			.role(adminRole)
            			.dateCreation(new Date())
            			.enable(true)
            			.build());
            }
            
            if(messageDao.count() == 0) {
            	messageDao.save(
            			Message.builder()
            			.value("1 er msg de l'admin")
            			.user(userAdmin)
            			.build());
            }
        };
    }
}
