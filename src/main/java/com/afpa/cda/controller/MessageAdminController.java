package com.afpa.cda.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.cda.dto.MessageDto;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.Message;
import com.afpa.cda.entity.User;
import com.afpa.cda.service.IMessageService;
import com.afpa.cda.service.IUserService;

@RestController
public class MessageAdminController {

	@Autowired
	private IMessageService messageService;

	@Autowired
	private IUserService userService;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping(path = { "msg" })
	public List<MessageDto> list() {
		return this.messageService.trouverTousLesMessages();
	}

	@PostMapping(path = { "msg" })
	public MessageDto add(@RequestParam String msg, Principal principal) {
		msg = msg.replaceAll("<", "&#x3C;");
		msg = msg.replaceAll(">", "&#x3E;");
		System.out.println(principal.getName());
		System.out.println(msg);
		Optional<UserDto> userDtoOp = userService.trouverParLeNomDUtilisateur(principal.getName());
		if (userDtoOp.isPresent()) {
			UserDto userDto = userDtoOp.get();
			Message message = Message.builder().value(msg).user(modelMapper.map(userDto, User.class)).build();
			return this.messageService.ajouter(modelMapper.map(message, MessageDto.class));
		}

		return null;
	}

}
