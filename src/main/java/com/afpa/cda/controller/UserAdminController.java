package com.afpa.cda.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.cda.dto.UserDto;
import com.afpa.cda.service.Hashage;
import com.afpa.cda.service.IRoleService;
import com.afpa.cda.service.IUserService;

@RestController
public class UserAdminController {
	@Autowired
	private IUserService userService;
	@Autowired
	private IRoleService roleService;
	
	
	@GetMapping(path = {"users"})
	public List<UserDto> list() {
		return this.userService.trouverTousLesUtilisateurs();
	}

	@PostMapping(path = {"users"})
	public UserDto add(@Param(value = "name") String name,@Param(value ="password") String password) {
		UserDto user =new UserDto();
		user.setName(name);
		user.setPassword(Hashage.hashSalt(password));
		user.setDateCreation(new Date());
		user.setEnable(false);
		System.out.println(name + password);
		return user;
	}
	
	@PutMapping(path = {"users/{idUser}"})
	public UserDto edit(@PathVariable int idUser,@Param(value = "name") String name,@Param(value = "password") String password) {
		Optional<UserDto> userDto = userService.trouverParLID(idUser);
		
		if(userDto.isPresent()) {
			UserDto user= userDto.get();
			user.setName(name);
			user.setPassword(Hashage.hashSalt(password));
		
		return this.userService.ajouter(user);
		}
		return null;
	}	
	
	@DeleteMapping(path = {"users/{idUser}"})
	public void delete(@PathVariable int idUser, HttpServletResponse response) {
		if(this.userService.trouverParLID(idUser).isPresent()) {
			this.userService.supprimerParLID(idUser);
		} else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
	}
}
