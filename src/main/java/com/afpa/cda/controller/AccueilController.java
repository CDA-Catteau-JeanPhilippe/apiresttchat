package com.afpa.cda.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccueilController {
	
	@GetMapping(path = {"/","index.html"})
	public ModelAndView accueil(ModelAndView mv) {
		mv.setViewName("msg/list");
		return mv;
	}
	
	@GetMapping(path = {"user.html"})
	public ModelAndView accueilUser(ModelAndView mv) {
		mv.setViewName("user/list");
		return mv;
	}
	
	@GetMapping(path = {"login.html"})
	public ModelAndView login(ModelAndView mv) {
		mv.setViewName("login");
		return mv;
	}
}