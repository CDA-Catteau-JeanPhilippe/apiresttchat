package com.afpa.cda.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
	private Integer id;
    private String name;
    private String password;
    private boolean enable;
    private Date dateCreation;
    private Date dateDerniereConnexion;
    private RoleDto role;
}
