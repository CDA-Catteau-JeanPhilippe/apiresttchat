package com.afpa.cda.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.UserRepository;
import com.afpa.cda.entity.User;

@Service
public class CdaUserDetailsService implements UserDetailsService {
 
    @Autowired
    private final UserRepository userDao;
    
    public CdaUserDetailsService(UserRepository userDao) {
        this.userDao = userDao;
    }
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        User user = userDao.findByName(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new CdaUserPrincipal(user);
    }
}
